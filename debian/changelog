python-chameleon (4.6.0-1) unstable; urgency=medium

  * New upstream version 4.6.0

 -- Akash Doppalapudi <akashdoppalapudi2001@gmail.com>  Sat, 04 Jan 2025 17:54:18 +0000

python-chameleon (4.5.4-2) unstable; urgency=medium

  * d/patches/0001-fix-sphinx-conf.patch:
    - Fix invalid value in sphinx docs configuration (Closes: #1090134)

 -- Akash Doppalapudi <akashdoppalapudi2001@gmail.com>  Mon, 30 Dec 2024 15:51:37 +0000

python-chameleon (4.5.4-1) unstable; urgency=medium

  * New upstream version 4.5.4
  * d/control: Bump Standards-Version to 4.7.0
  * d/rules: Modify PYBUILD_TEST_ARGS to enable previously disabled tests

 -- Akash Doppalapudi <akashdoppalapudi2001@gmail.com>  Mon, 15 Apr 2024 15:59:58 +0000

python-chameleon (4.5.2-2) unstable; urgency=medium

  * Source-only upload.

 -- Akash Doppalapudi <akashdoppalapudi2001@gmail.com>  Fri, 29 Mar 2024 08:43:48 +0000

python-chameleon (4.5.2-1) unstable; urgency=medium

  * New upstream version 4.5.2
  * New Maintainer (Closes: #980408)
  * d/control:
    - Add pytest and sphinx as Build-Depends to run tests and generate docs
    - Add pybuild-plugin-pyproject to Build-Depends
    - Change Maintainer name
    - Add a new package python-chameleon-doc for documentation
    - Bump Standards-Version to 4.6.2
    - Add Vcs-Browser and Vcs-Git links
    - Add Testsuite: autopkgtest-pkg-pybuild
    - Remove python3-pkg-resources from binary package deps since it's
      replaced upstream
  * d/copyright:
    - Update copyright year for upstream maintainers
    - Remove copyright stanza for non existent/obsolete files
    - Add copyright stanza for debian/* files
  * d/rules:
    - Add PYBUILD_TEST_ARGS
    - Run dh along with sphinxdoc
    - override_dh_installchangelogs to install CHANGES.rst
    - execute_before_dh_sphinxdoc to generate docs using sphinx
    - avoid installing the testsuite into the binary pkg.
  * Remove d/tests/control and use Testsuite 'autopkgtest-pkg-pybuild' instead
  * d/python-chameleon-doc.doc-base: Add doc-base for python-chameleon-doc

 -- Akash Doppalapudi <akashdoppalapudi2001@gmail.com>  Wed, 27 Mar 2024 10:53:29 +0000

python-chameleon (3.8.1-1) unstable; urgency=medium

  * QA upload.
  * New upstream release.
    Release supports Python 3.9. Closes: #973169
  * d/watch: Switch to GitHub, PyPI dropped the testsuite.
  * Switch to debhelper-compat
  * d/patches: Deleted, contained an empty series file.
  * d/dirs: Deleted, related to the removed documentation package.
  * d/control:
    - Add rules-Requires-Root: no
    - Update Standards-Version to 4.5.1
    - Bump debhelper to 13
    - Remove lines for documentation package.
    - Point homepage to GitHub repository.
    - Remove XS-Testsuite field.
  * d/copyright:
    - Change source field to GitHub repository
    - Use secure URI
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.

 -- Håvard Flaget Aasen <haavard_aasen@yahoo.no>  Fri, 27 Nov 2020 12:26:09 +0100

python-chameleon (3.6.2-2) unstable; urgency=medium

  * [tests] stop testing Python 2 version

 -- Paul Gevers <elbrus@debian.org>  Thu, 19 Sep 2019 21:29:41 +0200

python-chameleon (3.6.2-1) unstable; urgency=medium

  * QA upload.
  * Orphan the package. Closes: #869438.
  * Stop building the Python2 package. Closes: #937641.
  * Stop building the doc package.
  * Fix some lintian warnings. Closes: #810545, #810802.

 -- Matthias Klose <doko@debian.org>  Thu, 05 Sep 2019 07:39:45 +0200

python-chameleon (2.24-1) unstable; urgency=medium

  * New upstream release.  (Closes: #802130)
  * d/tests/control: Use simpler Test-Command syntax.
  * d/tests/all{,-3}: Removed.

 -- Barry Warsaw <barry@debian.org>  Wed, 28 Oct 2015 10:49:51 -0400

python-chameleon (2.22-1) unstable; urgency=medium

  * New upstream release.
  * d/control: Bump Standards-Version to 3.9.6 with no other changes needed.
  * d/watch: Use the pypi.debian.net redirector.
  * d/copyright: Update license information for PSF licensed files.
    (Closes: #752293)

 -- Barry Warsaw <barry@debian.org>  Tue, 02 Jun 2015 11:47:55 -0400

python-chameleon (2.16-4) unstable; urgency=medium

  * Team upload.
  * d/control: Add missing python-chameleon-doc Breaks/Replaces against
    python-chameleon. Thanks to Andreas Beckmann. Closes: #768205.

 -- Arnaud Fontaine <arnau@debian.org>  Wed, 12 Nov 2014 16:37:07 +0900

python-chameleon (2.16-3) unstable; urgency=medium

  * d/tests:
    - Add all-3 test for Python 3.
    - Simplify tests to just make sure the module is importable.

 -- Barry Warsaw <barry@debian.org>  Sat, 28 Jun 2014 16:01:31 -0400

python-chameleon (2.16-2) unstable; urgency=medium

  * d/control:
    - Add myself to Uploaders.
    - Suggest the python-chameleon-doc package instead of Recommending it.

 -- Barry Warsaw <barry@debian.org>  Mon, 23 Jun 2014 11:59:25 -0400

python-chameleon (2.16-1) unstable; urgency=medium

  * Team upload.

  [ Gediminas Paulauskas ]
  * debian/tests/control: correctly spell python-zope.i18n. Closes: #747388

  [ Barry Warsaw ]
  * New upstream release.
  * debian/patches/do_not_download_distribute.patch: Removed; obsolete.
  * debian/control:
    - Added python3-chameleon and python-chameleon-doc binary packages.
    - Added necessary Build-Depends for Python 3.
    - Added X-Python3-Version header.
    - Removed build dependency on python-van.pydeb (which isn't available
      for Python 3) and switched it to dh-python (for pybuild).
    - Updated python-chameleon binary package headers for better
      compatibility with pybuild.
  * debian/install -> debian/python-chameleon-doc.install
  * debian/rules: Convert to pybuild build system and add Python 3 support.

 -- Barry Warsaw <barry@debian.org>  Tue, 17 Jun 2014 09:57:18 -0400

python-chameleon (2.6.1-2) unstable; urgency=medium

  * Team upload.
  * debian/control:
    + Bump Standards-Version to 3.9.5.
      - debian/copyright: Fix Format URL as version 1.0 of the
        specification has been released.
    + Make Vcs-Svn URL canonical (lintian).
    + Add "XS-Testsuite: autopkgtest". Closes: #692667.
  * debian/tests/control:
    + Remove Features field not needed as per autopkgtest document.
  * debian/patches/do_not_download_distribute.patch:
    + Do not download distribute which was leading to FTBFS.
      Thanks to Sebastian Ramacher. Closes: #733431.

 -- Arnaud Fontaine <arnau@debian.org>  Thu, 06 Feb 2014 14:56:17 +0900

python-chameleon (2.6.1-1) unstable; urgency=low

  * Team upload.
  * New upstream release. Closes: #640112.
  * Switch from now deprecated dh_pycentral to dh_python2. Closes: #616997.
    + debian/rules:
      - Use dh_python2 instead of dh_pycentral.
      - Remove namespace hack as it's supposed to be handled by dh_python2.
    + debian/control:
      - Remove python-central from Build-Depends.
      - Bump python-all Build-Depends to 2.6.6-3~.
      - Remove XB-Python-Version field.
  * debian/install, debian/dirs:
    + Add reference manual to /usr/share/doc/python-chameleon/manual/
  * debian/rules:
    + Avoid renaming Makefile by passing explicitly --buildsystem to dh.
    + Add upstream changelog.
  * Switch to 3.0 (quilt) source format.
    + debian/source/options:
      - Ignore changes in *.egg-info directory otherwise building twice fails.
  * debian/control:
    + Add Homepage field.
    + Bump Standards-Version to 3.9.2. No changes needed.
    + Wrap Uploaders and Build-Depends fields.
    + Rename XS-Vcs-Svn to Vcs-Svn and add Vcs-Browser (now supported by dpkg).
    + Depends on python >= 2.7 as ordereddict is not available yet (#649895).
  * Switch debian/copyright to DEP5.
  * debian/tests/all:
    + Specify explicitly testfilter, otherwise van-py.deb translates package
      name to Chameleon.

 -- Arnaud Fontaine <arnau@debian.org>  Mon, 05 Dec 2011 10:30:48 +0900

python-chameleon (1.2.13-1) unstable; urgency=low

  * Team upload.
  * New upstream (Closes: #604986)
  * debian/control:
    - Bumped policy version 3.9.1 (no changes)..
    - Build-Depends: use python-all instead of python-all-dev.

 -- TANIGUCHI Takaki <takaki@debian.org>  Wed, 26 Jan 2011 17:43:07 +0900

python-chameleon (1.1.1-2) unstable; urgency=low

  * Add note in debian/copyright that src/chameleon/ast/* is under the
    copyright of Edgewall Software and is distubuted under a BSD style
    license.

 -- Brian Sutherland <brian@vanguardistas.net>  Sun, 31 Jan 2010 18:35:34 +0100

python-chameleon (1.1.1-1) unstable; urgency=low

  * Initial Packaging.

 -- Brian Sutherland <brian@vanguardistas.net>  Thu, 14 Jan 2010 16:10:04 +0100
